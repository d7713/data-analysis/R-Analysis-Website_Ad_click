# R - Website Ad click prediction - Analysis

### By: Andrew Wairegi

## Description
To identify the main variables that predict whether someone will click
on an Advert or not. On the kenyan entrepreneurs website. So that he is able to identify
those that are more likely to click on an Ad for his new course. This may also
enable him, to be able to increase the predictor variables strength. So that more people click
on the Ad, using the same variables.

[Open notebook]

## Setup/installation instructions
1. Find a local folder on your computer
2. Setup it up as a empty repository (using git init)
3. Clone this repository there into the local folder (using git clone https://...)
4. Open Rstudio
5. Open the file
7. Run the file (ensure the dataset is in the same folder) 

## Known Bugs
There are no known issues / bugs

## Packages/Technologies Used
1. R - The programming language
2. Data table - A High-performance Dataframe Package
3. Tidyverse - A Data Exploration & Visualization Package
4. Psych - A Psychometric statistics Package
5. Corrr - A Correlation Package

<br>

### License
Copyright © 2021 **Andrew Wairegi**
<br>
You are free to view, but not copy.

[Open notebook]: https://gitlab.com/d7713/data-analysis/R-Analysis-Website_Ad_click/-/blob/main/Website_Ad_click_analysis.Rmd
